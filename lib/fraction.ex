defmodule Fraction do
  @doc """
  The Fraction module contains functions for working with rational
  numbers.

  ## Examples
    iex(1)> Fraction.new(1, 4) |>
    ...>    Fraction.add(Fraction.new(1, 4)) |>
    ...>    Fraction.add(Fraction.new(1, 2)) |>
    ...>    Fraction.value()
    1.0

    iex(2)> Fraction.new(1, 4) |>
    ...>    IO.inspect() |>
    ...>    Fraction.add(Fraction.new(1, 4)) |>
    ...>    IO.inspect() |>
    ...>    Fraction.add(Fraction.new(1, 2)) |>
    ...>    IO.inspect() |>
    ...>    Fraction.value()
    1.0

  With the following output:
    ~f[1/4]
    ~f[1/2]
    ~f[1/1]
  """

  @enforce_keys [:num, :den]
  defstruct num: 1, den: 1 

  def new(num, den) when is_integer(num) and is_integer(den) do
    %Fraction{num: num, den: den} |> normalize
  end

  def add(%Fraction{num: n1, den: d}, %Fraction{num: n2, den: d}) do
    Fraction.new(n1 + n2, d) |> normalize
  end

  def add(%Fraction{num: n1, den: d1}, %Fraction{num: n2, den: d2}) do
    d = d1 * d2
    n = n1 * d2 + n2 * d1
    Fraction.new(n, d) |> normalize
  end

  def sub(%Fraction{num: n1, den: d}, %Fraction{num: n2, den: d}) do
    Fraction.new(n1 - n2, d) |> normalize
  end

  def sub(%Fraction{num: n1, den: d1}, %Fraction{num: n2, den: d2}) do
    d = d1 * d2
    n = n1 * d2 - n2 * d1
    Fraction.new(n, d) |> normalize
  end

  def mul(%Fraction{} = a, %Fraction{} = b) do
    Fraction.new(a.num * b.num, a.den * b.den) |> normalize
  end

  def div(%Fraction{} = a, %Fraction{} = b) do
    Fraction.mul(a, reciprocal(b))
  end

  def reciprocal(%Fraction{} = x) do
    Fraction.new(x.den, x.num)
  end

  def value(%Fraction{} = f) do
    f.num / f.den
  end

  def sigil_f(str, []) do
    [num, den] = String.split(str, "/") |> Enum.map(&String.to_integer/1)
    Fraction.new(num, den)
  end

  defp normalize(%Fraction{num: x, den: x}), do: %Fraction{num: 1, den: 1}
  defp normalize(%Fraction{} = f) do
    hcf = gcd(f.num, f.den)
    %Fraction{num: Kernel.div(f.num, hcf), den: Kernel.div(f.den, hcf)}
  end

  defp gcd(0, b), do: b
  defp gcd(a, b) do
    gcd(rem(b, a), a)
  end
end

defimpl Inspect, for: Fraction do
  def inspect(%Fraction{} = f, _opts) do
    "~f[#{f.num}/#{f.den}]"
  end
end
