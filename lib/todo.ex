defmodule Todo do
  @moduledoc """
  Todo list app based on Elixir in Action by Saša Juric.
  """

  defmodule Entry do
    defstruct date: nil, title: nil
  end

  def new(), do: MultiDict.new

  def add_item(todo_list, %Entry{} = entry) do
    MultiDict.add(todo_list, entry.date, entry)
  end

  def add_item(todo_list, {_, _, _} = date, title) do
    MultiDict.add(todo_list, date, %Entry{date: date, title: title})
  end

  def entries(todo_list) do
    Map.to_list(todo_list)
  end

  def entries(todo_list, date) do
    MultiDict.get(todo_list, date)
  end
end

defmodule MultiDict do
  @moduledoc """
  Map that stores multiple values per key.
  """

  def new(), do: %{}

  def add(dict, key, value) do
    Map.update(dict, key, [value],
      fn values -> [value | values] end
    )
  end

  def get(dict, key) do
    Map.get(dict, key)
  end
end
